// ------------------------------------------------------------------------------------------------
// ----------------------------------- CODE JS ----------------------------------------------------
// ------------------------------------------------------------------------------------------------

"use strict";

// Obtenir l'élément DOM pour les catégories par son identifiant
let display = document.getElementById("display");
let superHeros = document.getElementById("superHerosName");
let editeurs = document.getElementById("superHerosPublisher");
// Initialisation des tableaux vides pour stocker les supersHeros et les editeurs
const arrSuperHeros = [];
const arrEditeurs = [];

// ------------------------------------------------------------------------------------------------
// ------------------------ FONCTION POUR CREER UN ELEMENT DOM ------------------------------------
// ------------------------------------------------------------------------------------------------

function creatNode(element) {
  return document.createElement(element);
}

// ------------------------------------------------------------------------------------------------
// ----------FONCTION POUR AJOUTER UN ELEMENT ENFANT A UN ELEMENT PARENT DANS LE DOM --------------
// ------------------------------------------------------------------------------------------------

function append(parent, el) {
  return parent.appendChild(el);
}

// ------------------------------------------------------------------------------------------------
// ---FONCTION POUR VERIFIER ET AJOUTER UN ELEMENT DANS UN TABLEAU S'IL N'EST PAS DEJA PRESENT ----
// ------------------------------------------------------------------------------------------------

function testDouble(arrayString, string) {
  // Vérifie si l'élément n'est pas vide et s'il n'est pas déjà dans le tableau
  if (string != "" && arrayString.indexOf(string) == -1) {
    // Ajoute l'élément au tableau s'il est unique
    arrayString.push(string);
  }
}

// ------------------------------------------------------------------------------------------------
// ------------------------ AFFICHER TOUTES LES CARDS SUR LA PAGE ---------------------------------
// ------------------------------------------------------------------------------------------------

// Fectch pour récupérer les données à partir du fichier JSON
fetch("./superHeros.json")
  .then((response) => {
    // Convertir la réponse en JSON
    return response.json();
  })
  .then(function affichage(jsondata) {
    // Parcour chaque objet du tableau JSON
    let heros = jsondata.supersHeros;
    heros.map(function (data) {
      // Extraction des superHeros et vérifie les doublons avant de les ajouter
      const listHeros = data.name;
      testDouble(arrSuperHeros, listHeros);
      //Extraction des editeurs et vérifie les doublons avant de les ajouter
      const listEditeurs = data.biography.publisher;
      testDouble(arrEditeurs, listEditeurs);
      // Créations des élements DOM pour afficher les détails des cartes
      let bodyCard = creatNode("div");
      let image = creatNode("img");
      let name = creatNode("h2");
      let card = creatNode("div");
      // Définit le contenu des élements avec les données des cards
      image.innerHTML = `${data.images.sm}`;
      name.innerHTML = `${data.name}`;
      image.src = data.images.sm;
      // ajoute les elements créés à la structure de la carte et de la page
      append(display, card);
      append(card, bodyCard);
      append(bodyCard, image);
      append(bodyCard, name);
      // Ajoute des classes Bootstrap pour le style
      card.classList.add("card");
      bodyCard.classList.add("card-body");
      image.classList.add("card-img-top");
      name.classList.add("card-text");

      // ------------------------------------------------------------------------------------------------
      // -------------------------------------- PARTIE BONUS --------------------------------------------
      // --------------------- LORSQUE JE CLIQUE SUR UNE CARD IL ME L'OUVRE EN MODAL --------------------
      // ---------------------          SEULEMENT SUR LA PAGE D'ACCEUIL              --------------------
      // ------------------------------------------------------------------------------------------------
      card.addEventListener("click", function () {
        afficherModal(data);
      });

      function afficherModal(heroDonnee) {
        const modalBody = document.querySelector(".modal-body");
        modalBody.innerHTML = `
            <img src="${heroDonnee.images.sm}" class="img-fluid mb-3">
            <div>
            <h3 class="text-center text-white">${heroDonnee.name}</h3>
                ${Object.entries(heroDonnee.powerstats)
                  .map(([key, value]) => {
                    return `<p class="text-white"><strong>${
                      key.charAt(0).toUpperCase() + key.slice(1)
                    }:</strong> ${value}</p>`;
                  })
                  .join("")}
            </div>
        `;

        let heroModal = new bootstrap.Modal(
          document.getElementById("heroModal")
        );
        heroModal.show();
      }
    });

    // ------------------------------------------------------------------------------------------------
    // ----- AFFICHE LES CARDS PAR EDITEUR ET REAFFICHER TOUTES LES CARDS LORSQUE C'EST SUR ALL -------
    // ---------- DESACTIVE LE SELECT DE SUPER HERO LORSQU'UN EDITEURS EST SELECTIONNE ----------------
    // ------------------------------------------------------------------------------------------------

    document
      .getElementById("superHerosPublisher")
      .addEventListener("change", function () {
        // Le this fait appel à l'objet de la fonction
        const selectPublisher = this.value;
        // Permet d'enlever tout les éléments dans le display en mettant à la place une chaine vide
        display.innerHTML = "";
        // Condition pour désactiver le select de SuperHeros et pour le réactiver
        if (selectPublisher != "all") {
          document.getElementById("superHerosName").disabled = true;
          // Changement de couleur du background lorsqu'il est désactiver
          document.getElementById("superHerosName").style.backgroundColor =
            "grey";
        } else if (selectPublisher === "all") {
          document.getElementById("superHerosName").disabled = false;
          // Changement de couleur du background lorsqu'il est activer
          document.getElementById("superHerosName").style.backgroundColor =
            "white";
          // Parcour des données pour réafficher toutes les cards
          heros.forEach((data) => {
            let bodyCard = creatNode("div");
            let image = creatNode("img");
            let name = creatNode("h2");
            let card = creatNode("div");
            // Définit le contenu des élements avec les données des cards
            image.innerHTML = `${data.images.sm}`;
            name.innerHTML = `${data.name}`;
            image.src = data.images.sm;
            // ajoute les elements créés à la structure de la carte et de la page
            append(display, card);
            append(card, bodyCard);
            append(bodyCard, image);
            append(bodyCard, name);
            // Ajoute des classes Bootstrap pour le style
            card.classList.add("card");
            bodyCard.classList.add("card-body");
            image.classList.add("card-img-top");
            name.classList.add("card-text");
          });
        }
        // Déclaration de la variable donneeHeros
        const donneeHeros = jsondata.supersHeros;
        donneeHeros.forEach((hero) => {
          // Filtre les héros en fonction de l'éditeur sélectionné et crée les cards
          if (hero.biography.publisher === selectPublisher) {
            // Création des balises dans le HTML
            let card = creatNode("div");
            let img = creatNode("img");
            let name = creatNode("h2");
            let bodyCard = creatNode("div");
            // Définit le contenu des élements avec les données des cards
            img.src = hero.images.sm;
            name.textContent = hero.name;
            // ajoute les elements créés à la structure de la carte et de la page
            append(display, card);
            append(card, bodyCard);
            append(bodyCard, img);
            append(bodyCard, name);
            // Ajoute des classes Bootstrap pour le style
            card.classList.add("card");
            bodyCard.classList.add("card-body");
            img.classList.add("card-img-top");
            name.classList.add("card-title");
          }
        });
      });

    // ------------------------------------------------------------------------------------------------
    // ------------------------ TRI PAR ORDRE ALPHABETIQUE LES LISTES ---------------------------------
    // --------------- GESTION DE L'AFFICHAGE DES DOUBLONS DANS LE SELECT > OPTION --------------------
    // ------------------------------------------------------------------------------------------------

    arrSuperHeros.sort();
    arrEditeurs.sort();
    // Crée et ajoute les options name à l'élément select
    for (let j = 0; j < arrSuperHeros.length; j++) {
      let option1 = creatNode("option");
      option1.value = arrSuperHeros[j];
      option1.textContent = arrSuperHeros[j];
      append(superHeros, option1);
    }
    // Crée et ajoute les options d'editeur à l'élément select
    for (let i = 0; i < arrEditeurs.length; i++) {
      let option = creatNode("option");
      option.value = arrEditeurs[i];
      option.textContent = arrEditeurs[i];
      append(editeurs, option);
    }
  })
  // Log les erreurs s'il y en a
  .catch((error) => {
    console.log(error);
  });

// ------------------------------------------------------------------------------------------------
// ---------------------------------- DEBUT DE LA PARTIE MODAL ------------------------------------
// ------------------------------------------------------------------------------------------------

// Ajoute un écouteur d'événements sur le menu déroulant des super héros
superHeros.addEventListener("change", function () {
  // Récupère le nom du super héros sélectionné
  const heroName = this.value;
  // Effectue une requête fetch pour récupérer les données du fichier JSON des super héros
  fetch("./superHeros.json")
    // Convertit la réponse en JSON
    .then((response) => response.json())
    .then((data) => {
      // Trouve les détails du super héros sélectionné en cherchant dans le tableau des super héros
      const heroDonnee = data.supersHeros.find(
        (hero) => hero.name === heroName
      );
      // Si aucun super héros n'est trouvé, sort de la fonction
      if (!heroDonnee) return;

      // Sélectionne le corps de la modal pour y insérer les détails du super héros
      const modalBody = document.querySelector(".modal-body");
      // Construit le contenu HTML à injecter dans la modal, y compris l'image, le nom
      // et les statistiques du super héros
      modalBody.innerHTML = `
          <img src="${heroDonnee.images.sm}" class="img-fluid mb-3">
          <div>
          <h3 class="text-center text-white">${heroDonnee.name}</h3>
              ${Object.entries(heroDonnee.powerstats)
                .map(([key, value]) => {
                  // Key correspond au titre du powerStat et value à la valeur du PowerStat
                  // Object.entries permet de prendre tous les objets entrants et le .map permet de les parcourir
                  // Pour chaque titre du PowerStat, crée un paragraphe
                  // Le nom de la titre est capitalisé et sa valeur est affichée
                  // Utilisation du strong pour le gras des powerstats
                  return `<p class="text-white"><strong>${
                    // Prends les données de heroData et charA sert à prendre la lettre que l'on souhaite
                    // et slice pour faire une copie provisoire
                    key.charAt(0).toUpperCase() + key.slice(1)
                  }:</strong> ${value}</p>`;
                })
                // Permet d'enlever les espaces vides
                .join("")}
                </div>
          </div>
      `;

      // Crée un nouvel objet modal Bootstrap pour la modal avec l'ID 'heroModal'
      let heroModal = new bootstrap.Modal(document.getElementById("heroModal"));
      // Affiche la modal avec les PowerStats du super héros
      heroModal.show();
    })
    // Log les erreurs s'il y en a
    .catch((error) => {
      console.log(error);
    });
});

// ------------------------------------------------------------------------------------------------
// -------------------------------- DEBUT DE LA PARTIE LOADER -------------------------------------
// ------------------------------------------------------------------------------------------------

let loader;
// Fonction pour appeler le chargement avec un setTimeout de 3s
function myFunction() {
  loader = setTimeout(showPage, 2000);
}

// Pour afficher les éléments du Loader
function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

// Attendre que la page soit entièrement chargée avant d'exécuter showPage
window.addEventListener("load", function () {
  showPage();
});
// ------------------------------------------------------------------------------------------------
// ---------------------------------------- FIN ---------------------------------------------------
// ------------------------------------------------------------------------------------------------
