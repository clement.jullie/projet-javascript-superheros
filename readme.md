_DATE: 26/03/2024 AU 28/03/2024_
**DOCUMENTATION POUR L'ECF DE L'AFPA**
**FORMATION DWWM**
**FORMATEUR STEPHANE**

**_Mise en ligne du projet sur GitLab en public_**

https://gitlab.com/clement.jullie/projet-javascript-superheros.git

**_Liens CDN BOOTSTRAPS ET FONT-AWESOME_**

- Utilisation de Bootstraps 5.3.3
- Utilisation de Font-Awesome 6.5.1

**_OBJECTIFS_**

- L'exercice consiste à réaliser une application web permettant de rechercher des supers héros et d'afficher leurs caractéristiques dans une page de navigateur. Voici les détails des consignes :

**_Matériel fourni:_**

- Un dossier "SUPERS_HEROS" contenant :
- index.html
- styles.css
- app.js
- superHeros.json
- Une police d'écriture dans le dossier font/KOMIKAX\_.ttf

**_Résultat attendu:_**

- La page doit être responsive et s'adapter à trois formats d'affichage : mobile (375px), tablette (768px) et desktop.
- Deux listes déroulantes doivent être présentes :
  1-- La première contenant les noms de tous les supers héros.
  2-- La seconde listant le nom des éditeurs, sans doublons.
- En sélectionnant un super héros par son nom via la liste déroulante, une fenêtre modale doit apparaître affichant l'image du super héros, son nom, et ses caractéristiques "powerstats".
- Lorsqu'un super héros est sélectionné, la liste des éditeurs devient inactive. En sélectionnant "All" dans la liste des noms, toutes les cartes (cards) de supers héros apparaissent à l'écran, et la liste des éditeurs redevient active.
- En choisissant un éditeur dans la liste, la liste des noms devient inactive, et les cartes des supers héros associés à cet éditeur s'affichent sur l'écran.
- En sélectionnant "All" dans la liste des éditeurs, toutes les cartes réapparaissent, et la liste des noms devient active à nouveau.
- Compte tenu du poids important de la page due aux images, il est nécessaire d'implémenter un loader pour améliorer l'expérience utilisateur pendant le chargement des données.

\*\*\*\*\*\*\*\*\***_IMPORTANT_\***\*\*\*\*\*\*\*\*\

- Pour plus de détails sur les consignes se référer au pdf dans le dossier local: "Consignes_EvaluationConsignes_Evaluation_JavaScript.pdf"

\*\*\*POUR LA PARTIE DOCUMENTATION QUI M'A SERVI DURANT L'EVALUATION:\*\*\*\

**Aides à partir d'exercice déjà réalisés pour la partie Javascript:**
Aide avec les anciens exercices (Books, Carburants, photos).
Aide avec des exercices perso (Evenements Villes, portfolio)

**Documentation pour la partie rendre inactif un selected**
https://openclassrooms.com/forum/sujet/active-et-desactive-un-champ-select-49709

**Documentation pour les .show:**
https://developer.mozilla.org/fr/docs/Mozilla/Add-ons/WebExtensions/API/downloads/show

**Documentation pour les .find:**
https://www.google.com/search?q=.find+js+youtube&sca_esv=b7834c396069f319&rlz=1C1CHBD_frFR1043FR1043&sxsrf=ACQVn0-wkkrAOdH9Ng762O061CEUUnEzOw%3A1711531187039&ei=s-QDZrSkAcOnkdUPu-2q0A8&ved=0ahUKEwj0ybLDjpSFAxXDU6QEHbu2CvoQ4dUDCBA&uact=5&oq=.find+js+youtube&gs_lp=Egxnd3Mtd2l6LXNlcnAiEC5maW5kIGpzIHlvdXR1YmUyBhAAGAgYHjIGEAAYCBgeSJ4OUJICWIQNcAF4AZABAJgBR6ABzQGqAQEzuAEDyAEA-AEBmAIEoALbAcICChAAGEcY1gQYsAPCAgYQABgHGB7CAggQABgIGAcYHsICChAAGAgYBxgeGAqYAwCIBgGQBgiSBwE0oAeIFA&sclient=gws-wiz-serp#fpstate=ive&vld=cid:2c4fd97f,vid:DgROCYkg-lU,st:0

**Documentation pour la partie des valeurs retournées (return)**
https://developer.mozilla.org/fr/docs/Learn/JavaScript/Building_blocks/Return_values

**Documentation pour la partie 'error':**
https://stackoverflow.com/questions/69068272/uncaught-typeerror-cannot-read-properties-of-undefined-reading-replace

**Documentations pour la partie CSS, Bootstraps:**
https://getbootstrap.com/docs/5.3/utilities/display/
https://getbootstrap.com/docs/5.3/forms/form-control/
https://getbootstrap.com/docs/5.3/forms/select/

**Documentation pour les forms:**
https://javascript.developpez.com/faq/javascript/?page=formChamps

**Documentation pour les .this**
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/this

**Documentation pour les DOMContentLoaded_event**
https://developer.mozilla.org/fr/docs/Web/API/Document/DOMContentLoaded_event

**Documentation pour les Object.Entries**
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/entries

**Documentation pour les modals Bootstraps**
https://getbootstrap.com/docs/5.3/components/modal/#how-it-works
https://www.youtube.com/watch?v=hZB6MQ73_5M&ab_channel=Codeapick

**Documentation les while**
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Statements/while

**Documentation pour les .join()**
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/join

**Documentation pour les .slice()**
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/slice

**Documentations pour les innerHTML:**
https://www.devenir-webmaster.com/V2/TUTO/CHAPITRE/JAVASCRIPT/27-travailler-avec-innerHTML/
https://stackoverflow.com/questions/64971654/using-innerhtml-in-javascript-back-tick

**Documentation pour les loader:**
https://www.w3schools.com/howto/howto_css_loader.asp
